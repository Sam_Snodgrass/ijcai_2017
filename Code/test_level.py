import pathfinding

def makeIsSolid(solids):
    def isSolid(tile):
        return tile in solids
    return isSolid

def makeGetNeighbors(jumps,levelStr,visited,isSolid):
    maxX = len(levelStr[0])-1
    maxY = len(levelStr)-1
    jumpDiffs = []
    for jump in jumps:
        jumpDiff = [jump[0]]
        for ii in range(1,len(jump)):
            jumpDiff.append((jump[ii][0]-jump[ii-1][0],jump[ii][1]-jump[ii-1][1]))
        jumpDiffs.append(jumpDiff)
    jumps = jumpDiffs
    def getNeighbors(pos):
        dist = pos[0]-pos[2]
        pos = pos[1]
        visited.add((pos[0],pos[1]))
        below = (pos[0],pos[1]+1)
        neighbors = []
        if below[1] > maxY:
            return []
        if pos[2] != -1:
            ii = pos[3] +1
            jump = pos[2]
            if ii < len(jumps[jump]):
                if  not (pos[0]+pos[4]*jumps[jump][ii][0] > maxX or pos[0]+pos[4]*jumps[jump][ii][0] < 0 or pos[1]+jumps[jump][ii][1] < 0) and not isSolid(levelStr[pos[1]+jumps[jump][ii][1]][pos[0]+pos[4]*jumps[jump][ii][0]]):
                    neighbors.append([dist+1,(pos[0]+pos[4]*jumps[jump][ii][0],pos[1]+jumps[jump][ii][1],jump,ii,pos[4])])
                if pos[1]+jumps[jump][ii][1] < 0 and not isSolid(levelStr[pos[1]+jumps[jump][ii][1]][pos[0]+pos[4]*jumps[jump][ii][0]]):
                    neighbors.append([dist+1,(pos[0]+pos[4]*jumps[jump][ii][0],0,jump,ii,pos[4])])
                
        if isSolid(levelStr[below[1]][below[0]]):
            if pos[0]+1 <= maxX and not isSolid(levelStr[pos[1]][pos[0]+1]):
                neighbors.append([dist+1,(pos[0]+1,pos[1],-1)])
            if pos[0]-1 >= 0 and not isSolid(levelStr[pos[1]][pos[0]-1]):
                neighbors.append([dist+1,(pos[0]-1,pos[1],-1)])

            for jump in range(len(jumps)):
                ii = 0
                if not (pos[0]+jumps[jump][ii][0] > maxX or pos[1] < 0) and not isSolid(levelStr[pos[1]+jumps[jump][ii][1]][pos[0]+jumps[jump][ii][0]]):
                    neighbors.append([dist+ii+1,(pos[0]+jumps[jump][ii][0],pos[1]+jumps[jump][ii][1],jump,ii,1)])

                if not (pos[0]-jumps[jump][ii][0] < 0 or pos[1] < 0) and not isSolid(levelStr[pos[1]+jumps[jump][ii][1]][pos[0]-jumps[jump][ii][0]]):
                    neighbors.append([dist+ii+1,(pos[0]-jumps[jump][ii][0],pos[1]+jumps[jump][ii][1],jump,ii,-1)])

        else:
            neighbors.append([dist+1,(pos[0],pos[1]+1,-1)])
            if pos[1]+1 <= maxY:
                if not isSolid(levelStr[pos[1]+1][pos[0]+1]):
                    neighbors.append([dist+1.4,(pos[0]+1,pos[1]+1,-1)])
                if pos[0]-1 > 0 and not isSolid(levelStr[pos[1]+1][pos[0]-1]):
                    neighbors.append([dist+1.4,(pos[0]-1,pos[1]+1,-1)])
            if pos[1]+2 <= maxY:
                if not isSolid(levelStr[pos[1]+2][pos[0]+1]):
                    neighbors.append([dist+2,(pos[0]+1,pos[1]+2,-1)])
                if not isSolid(levelStr[pos[1]+2][pos[0]-1]):
                    neighbors.append([dist+2,(pos[0]-1,pos[1]+2,-1)])
        return neighbors
    return getNeighbors

def findPaths(subOptimal,solids,jumps,levelStr, maxX):
    visited = set()
    isSolid = makeIsSolid(solids)
    getNeighbors = makeGetNeighbors(jumps,levelStr,visited,isSolid)
    paths = pathfinding.astar_shortest_path( (2,12,-1), lambda pos: pos[0] == maxX, getNeighbors, subOptimal,lambda pos: 0)#lambda pos: abs(maxX-pos[0]))
    return [[ (p[0],p[1]) for p in path] for path in paths]

def testingPlayability(Jfile, mapName, mode):
    import json
    levelFilename = mapName+".txt"
    level = []
    with open(levelFilename) as level_file:
        for line in level_file:
            level.append(line.rstrip())
    with open(Jfile) as data_file:
        platformerDescription = json.load(data_file)
    maxX = len(level[0])-1
    paths=[]
    
    #print level
    
    #returns the furthest distance travelled by
    if mode == '1':
        while len(paths)==0 and maxX > 0:
            paths =  findPaths(10,platformerDescription['solid'],platformerDescription['jumps'],level, maxX)
            maxX = maxX-1
            
            if len(paths) > 0:
                return paths[0][len(paths[0])-1][0]
            if maxX == 1:
                return 0;

    if mode =='2':
        while len(paths)==0 and maxX > 0:
            paths =  findPaths(10,platformerDescription['solid'],platformerDescription['jumps'],level, maxX)
            maxX = maxX-1
            
            f = open(mapName+"_Annotated_Path.txt", 'w')
            f1= open(mapName+"_Path.txt", 'w')
            #toWrite = ([(p[0],p[1]) for p in paths[0]])
            
            
            #w = ""
            #get the path, and annotate the output map with that path
            if len(paths) == 0:
                return 0;
            
            for p in paths[0]:
                level[p[1]] = level[p[1]][:p[0]] + 'x' + level[p[1]][p[0]+1:];
                f1.write(str(p[1]) +'\t' + str(p[0])+'\n')
            
            for r in level:
                f.write(r+'\n')

            f.close();
            f1.close();
            if(len(paths[0]) > 0):
                return 1
            else:
                return 0

    if mode == '0':
        paths = findPaths(10,platformerDescription['solid'],platformerDescription['jumps'],level, maxX)
        return paths[0][len(paths[0])-1][0] if paths else 0
        #return len(paths[0]) if paths else 0


if __name__ == "__main__":
    import sys
    testingPlayability(sys.argv[1], sys.argv[2], sys.argv[3])
    #import sys
    #import json
    #if len(sys.argv)  < 4:
    #    print 'Usage: {} <platformer json> <level text filename> <full level (0) or as far as can go (1)>'.format(sys.argv[0])
    #    exit()

#levelFilename = sys.argv[2]
#level = []
#   with open(levelFilename) as level_file:
#       for line in level_file:
#           level.append(line.rstrip())
#   with open(sys.argv[1]) as data_file:
#       platformerDescription = json.load(data_file)
#   maxX = len(level[0])-1
#   paths=[]
    
    #returns the furthest distance travelled by
    #   if sys.argv[3] == '1':
    #   while len(paths)==0 and maxX > 0:
    #       paths =  findPaths(10,platformerDescription['solid'],platformerDescription['jumps'],level, maxX)
    #       maxX = maxX-1
    #       if len(paths) > 0:
    #           print paths[0][len(paths[0])-1][0]
    #           maxX =0
    #
    #if sys.argv[3] == '0':
    #   paths = findPaths(10,platformerDescription['solid'],platformerDescription['jumps'],level, maxX)
#   print len(paths)
#
