#include "MapReader.h"
#include<fstream>
#include<sstream>
#include<iostream>

MapReader::MapReader(){
}

void MapReader::CropMap(string InputFile, string OutputFile, int width, int height){
    
    ifstream Input;
    Input.open(InputFile.c_str());
    
    ofstream Output;
    Output.open(OutputFile.c_str());
    
    string line;
    vector<string> Rows;
    
    int MapH = 0;
    int MapW = 0;
    while (getline(Input, line)){
        MapH++;
        if(MapH != 12)
        line.pop_back();
        Rows.push_back(line.substr(0, line.length() - (line.length()%width)));
        
    }
    
    MapW = (int)line.length();
    
    for (int i = MapH%height; i < MapH; i++)
    Output << Rows[i] << endl;
    
    Input.close();
    Output.close();
}

void MapReader::GatherAllBlocks(string Folder, string InputMapNameBase, int NumberOfMaps, int width, int height){
    
    string InputFile;
    string OutputFile;
    string BlockFile;
    
    BlockFile = Folder+to_string(width) +"X" + to_string(height) + "_Blocks.txt";
    ofstream Gathered;
    Gathered.open(BlockFile.c_str());
    
    
    //gather all the blocks from the maps into 1 file
    for (int i = 1; i <= NumberOfMaps; i++){
        ifstream Input;
        Input.open(Folder+ InputMapNameBase + to_string(i) + ".txt");
        vector<string> Rows;
        string line;
        while (getline(Input, line)){
            for(int z=0; z<line.length(); z++){
                if(line.at(z) == '\n' || line.at(z) == '\r' || line.at(z) == '\t')
                line.erase(line.begin()+z);
            }
            
            Rows.push_back(line.substr(0,line.length()-(line.length()%width)));
        }
        int heightOver=Rows.size()%height;
        for(int j=0; j<heightOver; j++)
        Rows.erase(Rows.begin());
        
        for (int h = 0; h < Rows.size()/height; h++){
            for (int x = 0; x < Rows.at(h).length(); x += width){
                for (int y = 0; y < height; y++){
                    Gathered << Rows.at(y + height*h).substr(x, width) << endl;
                }
            }
        }
    }
}

//takes a tile map and converts it to the Hierarchical representation
bool MapReader::ConvertTileMapToHier(int width, int height, string InputMapName, string OutputMapName){
    
    ifstream InputMap;
    ofstream OutputMap;
    
    cout<<InputMapName<<endl;
    InputMap.open(InputMapName.c_str());
    OutputMap.open(OutputMapName.c_str());
    
    if(!InputMap)
        cout<<"Could not open file"<<endl;
    
    vector< vector < char > > Chunk;
    vector< char > GroundChunk;
    Chunk.resize(height);
    for (int i = 0; i < height; i++)
        Chunk.at(i).resize(width);
    
    GroundChunk.resize(width);
    vector< vector<char> > Map;
    string line;
    
    //read in the map
    while(getline(InputMap, line)){
        vector<char> tmp;
        for (int w = 0; w<(line.length()/width)*width; w++){
            if(line.at(w)!= '\r' && line.at(w)!= '\n' && line.at(w)!= '\t')
                tmp.push_back(line[w]);
        }
        Map.push_back(tmp);
        tmp.clear();
    }
    
    //portion at end of the map that doesn't fill a new block
    int ChunkRows = (int)Map.size()/height;
    if((Map.size()%height) > 0)
        ChunkRows++;
    int ChunkCols = (int)Map.at(0).size()/width;
    
    //loop through the chunked rows and cols of the map
    //and set the values of each chunk accordingly
    for (int r = 0; r<ChunkRows; r++){
        for (int c = 0; c<ChunkCols; c++){
            
            //for normal chunks
            if ((r < ChunkRows  && (Map.size()%height == 0)) || r < (ChunkRows-1)){
                //set the chunk
                for (int i = 0; i<height; i++){
                    for (int j = 0; j<width; j++)
                    Chunk[i][j] = Map.at(r*height + i).at(c*width + j);
                }
                
                int id = ClassifyChunk(Chunk, width, height);
                
                switch (id){
                        
                    case(2) :
                        OutputMap << 'E';
                        break;
                        
                    case(3) :
                        OutputMap << 'P';
                        break;
                        
                    case(4) :
                        OutputMap << 'I';
                        break;
                        
                    case(5) :
                        OutputMap << 'L';
                        break;
                        
                    case(6) :
                        OutputMap << 'T';
                        break;
                        
                    case(7) :
                        OutputMap << 'U';
                        break;
                        
                    case(8) :
                        OutputMap << 'D';
                        break;
                        
                    default:
                        OutputMap << '%';
                        break;
                }
            }
            //specifically for the ground chunks
            else {
                //set the GroundChunk
                for (int j = 0; j<width; j++)
                GroundChunk[j] = Map.at(r*height).at(c*width + j);
                
                if (isSolid(GroundChunk, width))
                OutputMap << 'G';
                else
                OutputMap << 'A';
            }
        }
        OutputMap << endl;
    }
    
    OutputMap.close();
    InputMap.close();
    
    return true;
}

void MapReader::ConvertAllTileMapsToHier(string InputMapNameBase, string InputFolder, string OutputFolder, int NumberOfMaps, int width, int height, vector<vector<vector<char> > > Medoids, float(*f) (vector< vector< char > >, vector< vector< char> >, vector< vector <float> >, vector<char>, int, int), string Comparison_Function, vector< vector< float> > TileSimilarities, vector<char> types){
    
    
    //cout<<"Tile hXw: "<<height<<" "<<width<<endl;
    
    string InputFile;
    string OutputFile;
    
    for(int i=1; i<=NumberOfMaps; i++){
        InputFile = InputFolder + InputMapNameBase + to_string(i) + ".txt";
        OutputFile = OutputFolder + InputMapNameBase + to_string(i) + ".txt";
        
        
        ifstream Input;
        ofstream Output;
        Input.open(InputFile.c_str());
        if(!Input)
            continue;
        Output.open(OutputFile.c_str());
        
        char Reps[51] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        vector< vector< char > > Current_Block;
        vector< vector< char > > Medoid_Block;
        vector< vector< char > > Map;
        
        //resize blocks to compare to the right size
        Current_Block.resize(height);
        for(int j = 0; j < height; j++)
            Current_Block[j].resize(width);
        
        //read in the entire current map
        string line;
        
        //added for Levi evaluation, add an extra row of empties at the top
        //to allow for an even 16 rows
        vector<char> t;
        /*for (int p=0; p<180; p++) {
            t.push_back('-');
        }
        Map.push_back(t);
        */
        
        while(!getline(Input, line).eof()){
            vector< char > tmp;
            for(int j=0; j<line.length(); j++){
                if(line.at(j)!= '\r' && line.at(j)!= '\n' && line.at(j)!= '\t')
                tmp.push_back(line.at(j));
            }
            Map.push_back(tmp);
        }
        
        vector< char > tmp;
        //cout<<line<<endl;
        //cout<<"After line"<<endl;
        for(int j=0; j<line.length(); j++){
            if(line.at(j)!= '\r' && line.at(j)!= '\n' && line.at(j)!= '\t')
                tmp.push_back(line.at(j));
        }
        if(tmp.size()>0)
            Map.push_back(tmp);
        
        //cut off the last few tiles of the map, if they do not make an entire block
        for(int j = 0; j < Map.size(); j++)
            Map[j].resize(Map[j].size() - Map[j].size()%width);
        
        bool first =true;
        for(int h = 0; h < Map.size()/height; h++){
            for(int w = 0; w < Map[0].size()/width; w++){
                
                // if the number of rows is not evenly divided by the height of the tiles,
                // then slide a window over the second and third row (only works if h=2)
                cout<<Map.size()%height<<" "<<Map.size()<<" "<<height<<" "<<h<<endl;
                if(Map.size()%height!=0 && h==1 && first){
                    for(int ht = h*height-1; ht < (h+1)*height-1; ht++){
                        for(int wt = w*width; wt < (w+1)*width; wt++){
                            Current_Block[ht - (h*height-1)][wt - w*width] = Map[ht][wt];
                            //cout<<Current_Block.size()<<" "<<Current_Block[0].size()<<endl;
                            //cout<<ht<<" "<<h<<" "<<height<<" "<<wt<<" "<<w<<" "<<width<<endl;
                        }
                    }
                    first=false;
                    h--;
                } else {
                    for(int ht = h*height; ht < (h+1)*height; ht++){
                        for(int wt = w*width; wt < (w+1)*width; wt++){
                            Current_Block[ht - h*height][wt - w*width] = Map[ht][wt];
                            //cout<<Current_Block.size()<<" "<<Current_Block[0].size()<<endl;
                            //cout<<ht<<" "<<h<<" "<<height<<" "<<wt<<" "<<w<<" "<<width<<endl;
                        }
                    }
                }
                
                float Lowest_Cost = 999999999;
                int Correct_Medoid=-1;
                for(int m = 0; m < Medoids.size(); m++){
                    Medoid_Block = Medoids[m];
                    float Cost = f(Medoid_Block, Current_Block, TileSimilarities, types, width, height);
                    if(Lowest_Cost > Cost){
                        Lowest_Cost = Cost;
                        Correct_Medoid = m;
                    }
                }
                Output<<Reps[Correct_Medoid];
            }
            Output<<endl;
        }
        Output.close();
    }
}

int MapReader::ConvertAllTileMapsToHier(string InputMapNameBase, string OutputMapNameBase, int NumberofMaps, int width, int height){
    
    string InputFile;
    string OutputFile;
    int NumConverted = 0;
    
    for (int i = 0; i < NumberofMaps; i++){
        InputFile  = InputMapNameBase  + to_string(i) + ".txt";
        OutputFile = OutputMapNameBase + to_string(i) + ".txt";
        
        if (ConvertTileMapToHier(width, height, InputFile, OutputFile))
            NumConverted++;
    }
    
    return NumConverted;
}

//classifies a regular chunk into 1 of 7 categories
//first checks for pillar, plateau, and slopes as they are mutually exclusive
//then checks for pipes, platforms, and empties
//in that order, and returns the first one seen
int MapReader::ClassifyChunk( vector<vector<char> > Chunk, int width, int height){
    
    
    if (isPillar(Chunk, height, width))
    return 4;
    
    if (isPlateau(Chunk, height, width))
    return 5;
    
    if (isUpSlope(Chunk, height, width))
    return 7;
    
    if (isDnSlope(Chunk, height, width))
    return 8;
    
    if (isPipe(Chunk, height, width))
    return 3;
    
    if (isPlatform(Chunk, height, width))
    return 6;
    
    if (isEmpty(Chunk, height, width))
    return 2;
    
    //should never return 9
    //error
    return 9;
}

//classifies the ground chunk as either solid or gap
int MapReader::ClassifyGroundChunk(vector<char> Chunk, int width){
    if (isSolid(Chunk, width))
    return 0;
    else
    return 1;
}

//Loops through the ground chunk given
//Tracks the biggest gap that appears in this chunk
//If a gap larger than 1 tile appears, return false
//only called on bottom groundChunks
//0
bool MapReader::isSolid(vector<char>  Chunk, int width){
    int BiggestGap = 0;
    int CurrentGap = 0;
    for (int i = 0; i<width; i++){
        if (Chunk[i] == '-')
        CurrentGap++;
        else
        CurrentGap = 0;
        
        if (CurrentGap > BiggestGap)
        BiggestGap = CurrentGap;
    }
    if (BiggestGap > 1)
    return false;
    
    return true;
}

//Returns true if gap larger than 1 appears
//returns false otherwise
//only called on bottom groundChunks
//1
bool MapReader::isGap(vector<char>  Chunk, int width){
    return !isSolid(Chunk, width);
}

//if there are more than width nonempty tiles, return false
//2
bool MapReader::isEmpty(vector< vector<char> >  Chunk, int height, int width){
    
    int NonEmpties = 0;
    for (int h = 0; h<height; h++){
        for (int w = 0; w<width; w++){
            if (Chunk[h][w] != '-'){
                NonEmpties++;
            }
        }
    }
    
    if (NonEmpties < width)
    return true;
    
    return false;
}

//Looks for complete pP pairs.
//Looks for incomplete pP pairs at the start and end of the chunk.
//3
bool MapReader::isPipe(vector< vector<char> >  Chunk, int height, int width){
    
    int PipePairs = 0;
    for (int h = 0; h<height; h++){
        for (int w = 0; w<width - 1; w++){
            if (Chunk[h][w] == 'p' && Chunk[h][w + 1] == 'P')
            PipePairs++;
        }
    }
    if (PipePairs > 0)
    return true;
    
    bool HalfPipe = false;
    for (int h = 0; h < height; h++){
        if (Chunk[h][0] == 'P' || Chunk[h][width - 1] == 'p'){
            HalfPipe = true;
            break;
        }
    }
    
    return HalfPipe;
}

//checks for ground tiles
//checks if a chunk of ground tiles is taller than it is wide
//makes some checks about height difference, which is
//required so as not to classify slope chunks as pillars
//4
bool MapReader::isPillar(vector< vector<char> >  Chunk, int height, int width){
    int BlockCount = 0;
    int W;
    int Hmin;
    int Hmax;
    int maxHeight = height;
    int minHeight = 0;
    int startWidth = width;
    int endWidth = 0;
    
    for (int i = 0; i<height; i++){
        for (int j = 0; j<width; j++){
            if (Chunk[i][j] == '#'){
                if (j < startWidth)
                startWidth = j;
                
                if (j >= endWidth)
                endWidth = j + 1;
                
                if (i < maxHeight)
                maxHeight = i;
                
                if (i > minHeight && Chunk[i - 1][j] != '#')
                minHeight = i;
                BlockCount++;
            }
        }
    }
    
    W = endWidth - startWidth;
    Hmax = height - maxHeight;
    Hmin = height - minHeight;
    //the pillar exists, H and W != 0
    //it's taller than it is wide
    if (W > 0 && Hmax > 0 && Hmax > W && BlockCount > 2){
        //if there is a height difference,
        if ((Hmax - Hmin) > 0){
            //it is no more than half the width
            if ((Hmax - Hmin) <= W / 2.0)
            return true;
            else
            return false;
        }
        //if no height difference, return true
        return true;
    }
    
    //if pillar does not exist, or is not taller than wide
    return false;
    
}

//checks for ground tiles
//checks if a chunk of ground tiles at least as wide as it is tall
//makes some checks about height difference, which is
//required so as not to classify slope chunks as plateaus
//5
bool MapReader::isPlateau(vector< vector<char> > Chunk, int height, int width){
    int BlockCount = 0;
    int W;
    int Hmin;
    int Hmax;
    int maxHeight = height;
    int minHeight = 0;
    int startWidth = width;
    int endWidth = 0;
    
    for (int i = 0; i<height; i++){
        for (int j = 0; j<width; j++){
            if (Chunk[i][j] == '#'){
                if (j < startWidth)
                startWidth = j;
                
                if (j >= endWidth)
                endWidth = j + 1;
                
                if (i < maxHeight)
                maxHeight = i;
                
                if (i > minHeight && Chunk[i - 1][j] != '#')
                minHeight = i;
                
                BlockCount++;
            }
        }
    }
    
    W = endWidth - startWidth;
    Hmax = height - maxHeight;
    Hmin = height - minHeight;
    //the plateau exists, H and W != 0
    //it's at least as wide as it it tall
    if (W > 0 && Hmax > 0 && Hmax <= W && BlockCount > 2){
        //if there is a height difference,
        if ((Hmax - Hmin) > 0){
            //it is no more than half the total height
            if ((Hmax - Hmin) < Hmax / 2.0)
            return true;
            else
            return false;
        }
        //if no height difference, return true
        return true;
    }
    
    //if plateau does not exist, or is not wider than tall
    return false;
}

//looks for platforms made of ?, #, and B tiles
//if length at least 2, return true
//6
bool MapReader::isPlatform(vector< vector<char> >  Chunk, int height, int width){
    
    int maxLength = 0;
    int PlatformLength = 0;
    for (int h = 0; h<height; h++){
        for (int w = 0; w<width; w++){
            if (Chunk[h][w] == '#' || Chunk[h][w] == 'B' || Chunk[h][w] == '?'){
                PlatformLength++;
                maxLength = PlatformLength;
            }
            else{
                PlatformLength = 0;
            }
        }
    }
    
    if (maxLength >= 2)
    return true;
    
    return false;
}

//looks for chunk of groung tiles
//checks if there is a significant slope
//returns true if there is, and it is upward
//7
bool MapReader::isUpSlope(vector< vector<char> >  Chunk, int height, int width){
    int W;
    int Hmin;
    int Hmax;
    int maxHeight = height;
    int minHeight = 0;
    int startWidth = width;
    int endWidth = 0;
    int minHeightPos=0;
    int maxHeightPos=0;
    
    for (int i = 0; i<height; i++){
        for (int j = 0; j<width; j++){
            if (Chunk[i][j] == '#'){
                if (j < startWidth)
                startWidth = j;
                
                if (j >= endWidth)
                endWidth = j + 1;
                
                if (i <= maxHeight){
                    maxHeight = i;
                    maxHeightPos = j;
                }
                
                if (i > minHeight && Chunk[i-1][j] != '#'){
                    minHeight = i;
                    minHeightPos = j;
                }
            }
        }
    }
    
    W = endWidth - startWidth;
    Hmax = height - maxHeight;
    Hmin = height - minHeight;
    //the slope exists, H and W != 0
    //and there is a height difference
    //and it is sloping upward
    if (W > 0 && Hmax > 0 && (Hmax - Hmin) > 0 && minHeightPos < maxHeightPos){
        //if wider than tall
        if (W > Hmax){
            if ((Hmax - Hmin) >= W / 3.0)
            return true;
        }
        //if a tall chunk
        else if (W <= Hmax){
            if ((Hmax - Hmin) >= Hmax / 3.0)
            return true;
        }
    }
    
    //if there is no height difference
    //or it is not sloping upwards
    return false;
}

//looks for chunk of groung tiles
//checks if there is a significant slope
//returns true if there is, and it is downward
//8
bool MapReader::isDnSlope(vector< vector<char> >  Chunk, int height, int width){
    int W;
    int Hmin;
    int Hmax;
    int maxHeight = height;
    int minHeight = 0;
    int startWidth = width;
    int endWidth = 0;
    int minHeightPos=0;
    int maxHeightPos=0;
    
    for (int i = 0; i<height; i++){
        for (int j = 0; j<width; j++){
            if (Chunk[i][j] == '#'){
                if (j < startWidth)
                startWidth = j;
                
                if (j >= endWidth)
                endWidth = j + 1;
                
                if (i <= maxHeight){
                    maxHeight = i;
                    maxHeightPos = j;
                }
                
                if (i > minHeight && Chunk[i-1][j] != '#'){
                    minHeight = i;
                    minHeightPos = j;
                }
            }
        }
    }
    
    W = endWidth - startWidth;
    Hmax = height - maxHeight;
    Hmin = height - minHeight;
    //the slope exists, H and W != 0
    //and there is a height difference
    //and it is sloping downward
    if (W > 0 && Hmax > 0 && (Hmax - Hmin) > 0 && minHeightPos > maxHeightPos){
        //if wider than tall
        if (W > Hmax){
            if ((Hmax - Hmin) >= W / 3.0)
            return true;
        }
        //if a tall chunk
        else if (W <= Hmax){
            if ((Hmax - Hmin) >= Hmax / 3.0)
            return true;
        }
    }
    
    //if there is no height difference
    //or it is not sloping downwards
    return false;
}

void MapReader::TryMapSplits(int width, int height, string InputMapFile, string Output){
    
    ifstream InputMap;
    ofstream OutputMap;
    
    InputMap.open(InputMapFile.c_str());
    OutputMap.open(Output.c_str());
    
    vector< vector<char> > Map;
    
    string line;
    
    //read in the map
    for (int h = 0; h<13; h++){
        getline(InputMap, line);
        vector<char> tmp;
        for (int w = 0; w<line.length(); w++){
            tmp.push_back(line[w]);
        }
        Map.push_back(tmp);
        tmp.clear();
    }
    
    //print the split map	
    for (int h = 0; h<13; h++){
        
        if (h%height == 0){
            for (int w = 0; w<Map.at(h).size() + (int)(Map.at(h).size() / width); w++)
            OutputMap << "=";
            OutputMap << endl;
        }
        
        for (int w = 0; w<Map.at(0).size(); w++){
            if (w%width == 0)
            OutputMap << "|";
            
            OutputMap << Map.at(h).at(w);
        }
        OutputMap << endl;
    }
    OutputMap.close();
    InputMap.close();
}